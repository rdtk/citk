CCA-LWR is a system that combines a kinematics simulator of the KUAK Lightweight Robot IV (LBR IV), and
a collection of libraries and tools to i) develop controller components with the compliant control
architecture (CCA), ii) develop control processes with the robotics service bus (RSB), iii) log, monitor
and introspect (rsb tools), and iv) record and replay experiments (rsbag tools).