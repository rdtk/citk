In this particular system version, iCub related tools (yarpview, iCubGui and dataSetPlayer) are used replay a
pre-recorded sequence as provided in the iCub Wiki in order to visualize the original state of the robot. Feel
free to 1) replicate the system and 2) execute the linked experiment to verify our results. Please see Linked
Artifacts section.