catalog:
  title: whisper models

templates:
- freestyle
- base

variables:
  description: |
    Whisper is a general-purpose speech recognition model. It is trained on a large dataset of diverse audio and is also a multitasking model that can perform multilingual speech recognition, speech translation, and language identification. Below are the names of the available models and their approximate memory requirements and relative speed.
    | Size   | Parameters | Required VRAM | Relative speed |
    |--------|------------|---------------|----------------|
    | tiny   | 39 M       | ~1 GB         | ~32x           |
    | base   | 74 M       | ~1 GB         | ~16x           |
    | small  | 244 M      | ~2 GB         | ~6x            |
    | medium | 769 M      | ~5 GB         | ~2x            |
    | large  | 1550 M     | ~10 GB        | 1x             |
    The .en models for English-only applications tend to perform better, especially for the tiny.en and base.en models. 

    Download links from https://github.com/openai/whisper/blob/main/whisper/__init__.py#L17

  recipe.maintainer:
  - lruegeme@techfak.uni-bielefeld.de

  whisper.model-url: "https://openaipublic.azureedge.net/main/whisper/models/${whisper.model-sha}/${version-name}.pt"

  shell.command: |
    #!/bin/bash
    unset DISPLAY
    mkdir -p ${toolkit.dir}/share/whisper-models
    wget -P ${toolkit.dir}/share/whisper-models ${whisper.model-url}
    
    if sha256sum ${toolkit.dir}/share/whisper-models/${version-name}.pt | grep -Eo '^\w+' | cmp -s <(echo ${whisper.model-sha}) ; then
        echo "Download succeeded"
    else
        echo "wrong checksum"
        rm ${toolkit.dir}/share/whisper-models/${version-name}.pt
        false
    fi

versions:
- name: "base.en"
  variables:
    whisper.model-sha: 25a8566e1d0c1e2231d1c762132cd20e0f96a85d16145c3a00adf5d1ac670ead
   

- name: "tiny.en"
  variables:
    whisper.model-sha: d3dd57d32accea0b295c96e26691aa14d8822fac7d9d27d5dc00b4ca2826dd03

- name: "tiny"
  variables:
    whisper.model-sha: 65147644a518d12f04e32d6f3b26facc3f8dd46e5390956a9424a650c0ce22b9

- name: "base"
  variables:
    whisper.model-sha: ed3a0b6b1c0edf879ad9b11b1af5a0e6ab5db9205f891f668f8b0e6c6326e34e

- name: "small.en"
  variables:
    whisper.model-sha: f953ad0fd29cacd07d5a9eda5624af0f6bcf2258be67c92b79389873d91e0872

- name: "small"
  variables:
    whisper.model-sha: 9ecf779972d90ba49c06d968637d720dd632c55bbf19d441fb42bf17a411e794

- name: "medium.en"
  variables:
    whisper.model-sha: d7440d1dc186f76616474e0ff0b3b6b879abc9d1a4926b7adfa41db2d497ab4f

- name: "medium"
  variables:
    whisper.model-sha: 345ae4da62f9b3d59415adc60127b97c714f32e89e936602e85993674d08dcb1

- name: "large-v2"
  variables:
    whisper.model-sha: 81f7c96c852ee8fc832187b0132e569d6c3065a3252ed18e56effd0b6a73e524

- name: "large-v1"
  variables:
    whisper.model-sha: e4b87e7e0bf463eb8e6956e646f1e277e901512310def2c24bf0e11bd3c28e9a