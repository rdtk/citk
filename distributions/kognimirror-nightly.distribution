catalog:
  title: Kognimirror
  version: nightly

variables:
  access: private
  variant: nightly
  datadir: /vol/csra/data/${variant}/

  platform-requires:
    ubuntu:
      packages:
      - cmake
      - build-essential
      - libprotobuf-dev
      - protobuf-compiler
      - libboost-dev
      - python-setuptools
      - python-dev
      - python-protobuf
      - libboost-thread-dev
      - libboost-filesystem-dev
      - libboost-signals-dev
      - libboost-program-options-dev
      - libboost-system-dev
      - libboost-regex-dev
      - libpcl-all
      - libffi-dev
      - libturbojpeg
      - libssl-dev
      - libglfw3-dev
      - beignet-dev
      - libopenni2-dev
      - libpng12-dev
      - libeigen3-dev
      - libjpeg-turbo8-dev

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}"

versions:
- kognidoc                                                  @master
- freenect2                                                 @master
- speech-recognition-python                                 @master
- kognimirror-openface                                      @master
- openface                                                  @master
- torch                                                     @master
- dlib-python                                               @master
- kogniserver                                               @master
- kognimirror-icl                                           @master
- kognimirror-gui                                           @master
- kognimirror-dialog                                        @master
- kognihome-displayserver-extras                            @master
- kognihome-tools                                           @master
- kognicom                                                  @master
- yaml-cpp                                                  @release-0.5.3
- icl-kognihome                                             @trunk
- lsp-csra-system-startup                                   @master
- lsp-csra-system-config                                    @master
- bullet                                                    @master
- vdemo                                                     @master
- kogniconfig                                               @master
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- rsc                                                       @0.15
- rsb-protocol                                              @0.15
- rsb-cpp                                                   @0.15
- rsb-python                                                @0.15
- rsb-java                                                  @0.15
- rsb-spread-cpp                                            @0.15
- rst-proto-csra-workaround                                 @0.15
- rst-proto-kognihome                                       @0.15
- rst-converters-cpp                                        @0.15
- rst-converters-python                                     @0.15
- rsb-tools-cpp                                             @0.15
- rsb-tools-cl-binary                                       @0.15
- rsbag-tools-cl-binary                                     @0.15
