catalog:
  title: Kognichef
  version: nightly

variables:
  access: private

  platform-requires:
    ubuntu:
      packages:
      - cmake
      - build-essential
      - libprotobuf-dev
      - protobuf-compiler
      - libboost-dev
      - python-setuptools
      - python-dev
      - python-protobuf
      - libprotobuf-java
      - libboost-thread-dev
      - libboost-filesystem-dev
      - libboost-signals-dev
      - libboost-program-options-dev
      - libboost-system-dev
      - libboost-regex-dev
      - maven
      - openjdk-7-jdk
      - openjdk-8-jdk
      - openjfx
      - libpcl-all
      - nodejs
      - nodejs-legacy
      - npm
      - libffi-dev

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}"

versions:
- kogniserver                                               @master
- kogniperception-kognichef-csra                            @csra
- kognichef-object-detection-csra                           @csra
- kognichef-fillrate-detection-csra                         @csra
- kognichef-presence-detection-csra                         @csra
- kognichef-fillrate-server-csra                            @csra
- kognichef-kinect-server-csra                              @csra
- kognichef-forcesensor-server-csra                         @csra
- kognichef-frontend-csra                                   @csra
- kogniconfig                                               @csra-kitchen
- kognihome-tools                                           @master
- kognicom                                                  @master
- yaml-cpp                                                  @release-0.5.3
- kognirecipes                                              @master
- mieleappliances                                           @csra
- kognichef-controller                                      @csra
- kognichef-controller-vis                                  @master
- icl-github                                                @kognihome
- bullet                                                    @master
- vdemo                                                     @master
- kognichef-dialogflow                                      @csra
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- rsc                                                       @0.17
- rsb-protocol                                              @0.17
- rsb-cpp                                                   @0.17
- rsb-python                                                @0.17
- rsb-java                                                  @0.17
- rsb-matlab                                                @0.17
- rsb-spread-cpp                                            @0.17
- rsb-gstreamer                                             @release-0.17
- rst-proto                                                 @0.17
- rst-experimental-proto                                    @0.17
- rst-converters-cpp                                        @0.17
- rst-converters-python                                     @0.17
- rsb-xml-cpp                                               @0.17
- rsb-xml-java                                              @0.17
- rsb-xml-python                                            @0.17
- rsb-scxml-engine                                          @csra-0.17
- rsb-tools-cpp                                             @0.17
- sbcl                                                      @sbcl-1.3.11
- quicklisp                                                 @2017-08-30
- cl-launch                                                 @4.1.3
- cl-iterate-sequence                                       @master
- cl-architecture.service-provider                          @release-0.4
- cl-architecture.builder-protocol                          @release-0.8
- cl-network.spread                                         @0.3
- rosetta-cl                                                @0.2
- rosetta-esrap-util-cl                                     @0.2
- rosetta-yarp-cl                                           @0.2
- rosetta-ros-cl                                            @0.2
- rsb-cl                                                    @0.17
- cl-protobuf                                               @0.2
- rsbag-cl                                                  @0.17
- rsb-yarp-cl                                               @0.17
- dlib                                                      @v19.2
- gazetool                                                  @nogui-rsb
