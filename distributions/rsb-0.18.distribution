catalog:
  title: Robotics Service Bus
  version: nightly
  description: opensource://rsb-generic.md
  image:
  - catalog://RSB_0.png

variables:
  recipe.maintainer:
  - flier@techfak.uni-bielefeld.de
  - jwienke@techfak.uni-bielefeld.de
  - jmoringe@techfak.uni-bielefeld.de
  - swrede@techfak.uni-bielefeld.de

  rosetta-version: '0.3'
  rsb-version: '0.18'

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/toolkit/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    rm -rf "${toolkit.dir}"
    mkdir -p "${toolkit.dir}"

versions:
- spread                                                    @5.0
- spread-python                                             @1.5spread4

- sbcl                                                      @sbcl-1.4.10
- quicklisp                                                 @2018-08-31
- cl-launch                                                 @4.1.3
- cl-iterate-sequence                                       @master
- cl-architecture.service-provider                          @0.5
- cl-architecture.builder-protocol                          @0.9
- cl-network.spread                                         @0.3
- cl-protobuf                                               @0.2
- cl-protocl                                                @master
- cl-serialization.protocol-buffer                          @master
- cl-dot                                                    @master

- rosetta-cl                                                @${rosetta-version}
- rosetta-esrap-util-cl                                     @${rosetta-version}
- rosetta-yarp-cl                                           @${rosetta-version}
- rosetta-ros-cl                                            @${rosetta-version}
- rosetta-protobuf-cl                                       @${rosetta-version}
- rosetta-tools-cl                                          @${rosetta-version}

- rsc                                                       @${rsb-version}
- rsb-protocol                                              @${rsb-version}
- rsb-cpp                                                   @${rsb-version}
- rsb-cl                                                    @${rsb-version}
- rsb-java                                                  @${rsb-version}
- rsb-python                                                @${rsb-version}
- rsb-matlab                                                @${rsb-version}
- rsb-tools-cpp                                             @${rsb-version}
- rsb-tools-cl                                              @${rsb-version}
- rsb-manual                                                @${rsb-version}
- rsb-tutorials                                             @${rsb-version}
- rsb-spread-cpp                                            @${rsb-version}
- rsb-ros-cl                                                @${rsb-version}
- rsb-yarp-cl                                               @${rsb-version}
- rsb-xml-cpp                                               @${rsb-version}
- rsb-xml-java                                              @${rsb-version}
- rsb-xml-python                                            @${rsb-version}
- rsb-xml-cl                                                @${rsb-version}

- rst-proto                                                 @${rsb-version}
- rst-converters-cpp                                        @${rsb-version}
- rst-converters-python                                     @${rsb-version}
- rst-manual                                                @${rsb-version}

- rsbag-cl                                                  @${rsb-version}
- rsbag-tools-cl                                            @${rsb-version}

- rsb-process-monitor                                       @1.0
- rsb-performance-test-api                                  @0.4
