variables:
  scm.trigger-disable?: ${next-value|${external?|false}}
  junit.allow-empty-results?: true
  email-notification.recipients: [rhaschke, gwalck]
  email-notification.send-to-perpetrator?: true
  access: private
  ros: ${next-value|melodic}
  ros.install.prefix: ${toolkit.dir}/ros/${ros}

  c*flags: ${next-value|-march=core2 -O3 -Wno-deprecated-declarations}
  ldflags: ' '

  toolkit.volume: /vol/famula
  toolkit.dir: ${toolkit.volume}/stable

  orchestration.success-results: [ SUCCESS, UNSTABLE ]
  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    rm -rf "${toolkit.dir}"/*
    for d in include lib bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    rosdep update

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: chmod -R a+rX,g-w,o-w "${toolkit.dir}"/*

  restrict-to-slaves: bionic
  shell.environment:
  - '@{next-value|[]}'

  cuda.dir: /vol/cuda/9.0
  cuda.host.compiler: \$(which g++-6)
  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${cuda.dir}
    - CUDA_NVCC_FLAGS="--expt-relaxed-constexpr"
    - '@{next-value|[]}'

versions:
- name: rsc
  version: famula-0.15
  parameters:
    external?: true
- name: rsc-log4cxx
  version: master
  parameters:
    external?: true
- name: rsb-protocol
  version: '0.15'
  parameters:
    external?: true
- name: rsb-cpp
  version: famula-0.15
  parameters:
    external?: true
- name: rsb-python
  version: '0.15'
  parameters:
    external?: true
- name: rsb-tools-cpp
  version: '0.15'
  parameters:
    external?: true
- name: spread
  version: '4.4'
  parameters:
    external?: true
- name: spread-python
  version: 1.5spread4
  parameters:
    external?: true
- name: rsb-spread-cpp
  version: famula-0.15
  parameters:
    external?: true
- name: xmltio
  version: master
  parameters:
    external?: true
- name: rsb-xml-cpp-xmltio
  version: '0.15'
  parameters:
    external?: true
- name: rsb-xml-python
  version: '0.15'
  parameters:
    external?: true
- name: rst-proto-famula
  version: '0.15'
  parameters:
    external?: true
    cmake.options:default: [ "@{next-value|[]}", "BUILD_JAVA=OFF"]
- name: rst-converters-cpp
  version: '0.15'
  parameters:
    external?: true
- name: rst-converters-python
  version: '0.15'
  parameters:
    external?: true
- name: rsb-process-monitor
  version: master
  parameters:
    external?: true
- name: rsb-opencv
  version: master
  parameters:
    external?: true
- kdl                                                       @master-ni
- name: cbf
  versions:
  - version: master
  - version: '0.3'
- name: icl-nivision
  version: master
  parameters:
    external?: true
    cmake.options:
    - '@{next-value|[]}'
    - BUILD_WITH_RSB=FALSE
    - BUILD_WITH_RSC=FALSE
    - BUILD_WITH_RST=FALSE
- sfb-c4-vision                                             @master
- dmxGui                                                    @master
- name: vdemo
  version: master
  parameters:
    external?: true
- name: pyscxml
  version: v.0.8.5-fsmt
  parameters:
    external?: true
- name: fsmt
  version: '0.19'
  parameters:
    external?: true
- rosbag                                                    @remote_record-kinetic
- name: rosbag-remote-record
  version: master
  parameters:
    external?: true
- name: libreflexxes
  version: 1.2.6
  parameters:
    external?: true
- name: humotion
  version: master
  parameters:
    external?: true
- name: xsc3
  version: master
  parameters:
    external?: true
- name: flobi-description
  version: master
  parameters:
    external?: true
- name: flobi-sim
  version: master
  parameters:
    external?: true
- name: hlrc_server
  version: master-ros-only
  parameters:
    external?: true
- name: hlrc_client_cpp
  version: master
  parameters:
    external?: true
- name: hlrc_client_python
  version: master
  parameters:
    external?: true
- name: hlrc_tts_provider
  version: master
  parameters:
    external?: true
- name: xcf_to_flobi_tts
  version: master
  parameters:
    external?: true
- name: python3-rospkg
  version: 1.0.37
  parameters:
    external?: true
- name: python3-catkin-pkg
  version: 0.2.10
  parameters:
    external?: true
- name: marytts
  version: v5.2
  parameters:
    external?: true
- name: marytts-voices
  version: '5.1'
  parameters:
    external?: true
- name: opencv-plus-contrib-cuda
  version: 3.4.8
  parameters:
    cudagen: ' '
- ximea-api                                                 @master
- ximea-camera                                              @devel-indigo
- image-gpu-ros                                             @master
- bart                                                      @opencv3
- bart_ros                                                  @opencv3
- name: cmusphinx-sphinxbase
  version: master
  parameters:
    external?: true
- name: cmusphinx-pocketsphinx
  version: master
  parameters:
    external?: true
- name: pocketsphinx-adapter
  version: agni
  parameters:
    external?: true
- name: ice
  version: 3.4.2
  parameters:
    external?: true
- xmltio-ni                                                 @master
- name: xcf
  version: master
  parameters:
    external?: true
- name: boost_threadpool
  version: 0.2.5
  parameters:
    external?: true
- name: xcf-am
  version: master
  parameters:
    external?: true
- name: xcf-jam
  version: '1.2'
  parameters:
    external?: true
- name: xcf-tools
  version: trunk
  parameters:
    external?: true
- ni-logger                                                 @master
- hsm                                                       @master
- spacenavi                                                 @master
- spacenav_rtt                                              @ubi-kinetic-devel
- libSTP                                                    @master
- ni-hand-server                                            @master
- pa10                                                      @master
- ni-arm-server                                             @master
- myrmex                                                    @master
- tactile-filters                                           @master
- tactile-glove                                             @master
- tactile_bracelet                                          @master
- teensy-tactile                                            @master
- agni_serial_protocol                                      @master
- labjack-exodriver                                         @cmake_build
- active_nail_driver                                        @master
- fts-saysrv                                                @sz-fts-gst
- pamini-sfb-c4                                             @master
- gefen-matrix                                              @master
- rsb-to-mem                                                @master
- ros-xcf-bridge                                            @master
- sayserver-auto-mute                                       @master
- xcf-grab-screenshot                                       @master
- xcf-robot-stop-button                                     @master
- urdfdom_headers                                           @sensor-refactoring
- urdfdom                                                   @ros-sensor-refactoring
- urdf                                                      @sensor-parsing
- tactile-toolbox                                           @melodic-devel
- pwlf                                                      @latest
- gazebo-tactile-plugins                                    @kinetic-devel
- rviz                                                      @agni-melodic-devel
- moveit                                                    @agni-melodic-devel
- moveit_online_collision_predictor                         @ubi-kinetic-devel
- moveit_task_constructor                                   @agni-master
- mtc_demos                                                 @master
- schunk_descriptions                                       @ubi-indigo-devel
- pa10_7a_description                                       @master
- pa10_7c_description                                       @master
- human_hand_description                                    @master
- agni_robots                                               @kinetic-devel
- sdh-rsb-grasping                                          @master
- name: libglfw3
  version: latest
  parameters:
    external?: true
- name: librealsense
  version: v2.10.1
  parameters:
    external?: true
- name: ros-realsense
  version: fix-cmake
  parameters:
    external?: true
- sr_config                                                 @shadowrobot_130704_indigo
- sr_visualization                                          @ubi-kinetic-devel
- sr_common                                                 @ubi-kinetic-devel
- sr_tools                                                  @ubi-kinetic-devel
- sr_teleop                                                 @ubi-kinetic-devel
- cereal_port                                               @master
- sr_core                                                   @ubi-melodic-devel
- shadow_robot_ethercat                                     @ubi-melodic-devel
- sr_contrib                                                @melodic-devel
- ros_ethercat                                              @ubi-melodic-devel
- rtt_myrmex_driver                                         @indigo-devel
- ros-serial                                                @main
- flir_ptu                                                  @master
- agni_tf_tools                                             @master
- tactile_marker_publisher                                  @master
- rtt_sr_bridge                                             @kinetic-devel
- rtt_sr_effort_limiter                                     @kinetic-devel
- rtt_sr_state_controller                                   @master
- iclros_config_parser                                      @master
- rtt_tactile_msgs                                          @indigo-devel
- iclros_bridge                                             @master
- iclros_remote_recorder                                    @master
- icl_ros_segmentation                                      @automatica18
- icl_ros_classification                                    @master
- icl_ros_robot_frame_calibration                           @master
- segmentation_msgs                                         @master
- icl_ros_launch                                            @master
- agni_marker_publishers                                    @master
- worldbelief_ps_updater                                    @master
- agni_touch_detection                                      @master
- agni_slip_detection                                       @master
- agni_robot_viz                                            @kinetic-devel
- agni_sr_effort_limiter                                    @master
- cart_lin_filter                                           @indigo-devel
- effort_limiter                                            @indigo-devel
- cd_dynamics                                               @indigo-devel
- agnirtt_tools                                             @kinetic-devel
- rtt_dot_service                                           @ubi-agni-kinetic-devel-bionic
- famula_lib_cbf                                            @master
- famula_active_inspection                                  @master
- famula_lib_robot_desc                                     @kinetic-devel
- orocos_toolchain                                          @toolchain-2.9-bionic
- rtt_ros_integration                                       @toolchain-2.9
- orocos_controllers                                        @ubi-kinetic-devel
- cartesian_trajectory_msgs                                 @stable
- orocos_common_controllers                                 @kinetic-devel
- lwr_hardware                                              @indigo-devel
- openkc                                                    @trunk
- lwr_panel_driver                                          @indigo-devel
- orocos_tools                                              @ubi-indigo-devel
- lwr_robot                                                 @ubi-kinetic-devel
- control_state_msgs                                        @master
- rtt_control_state_msgs                                    @master
- rtt_lwr_state_controller                                  @master
- rtt_tactile_controllers                                   @master
- agni_setup                                                @kinetic-devel
- ati_sensor                                                @ubi-kinetic-devel
- ati_sensor_description                                    @master
- gazebo_attach_links                                       @kinetic-devel
- plugin_axis_display                                       @kinetic-devel
- plugin_line_display                                       @kinetic-devel
- rtt_ati_sensor                                            @ubi-indigo-devel
- s_log_saver                                               @indigo-devel
- motion_generator                                          @master
- gazebo_attach_controller                                  @indigo-devel
- agni_joint_trajectory_generator                           @master
- agni_cbf_cartesian_controller                             @master
- s_motion_manager                                          @indigo-devel
- rtt_ptu_controller                                        @indigo-devel
- trajectory_filter                                         @indigo-devel
- flwr_filter                                               @kinetic-devel
- cartesian_filter                                          @master
- wearhap                                                   @master
- vicon_bridge                                              @master
- agni_softness_classifier                                  @master
- agni_manip_utils                                          @master
- agni_grasp_generator                                      @master
- agni_grasp_manager                                        @master
- grasping_msgs                                             @master
- object_fitter                                             @master
- sq_fitting_ros                                            @master
- sq_fitting                                                @master
- grasp_visualization                                       @master
- pyhsm                                                     @master
- pyhsm_msgs                                                @master
- ni-system-startup                                         @master
- agni_robot_utils                                          @master
- name: cython
  version: 0.28.x
  parameters:
    external?: true
- name: libgpuarray
  version: master
  parameters:
    external?: true
- name: theano
  version: rel-1.0.4
  parameters:
    external?: true
- name: tensorflow
  version: 1.11.0
  parameters:
    external?: true
- name: keras
  version: 2.1.3
  parameters:
    external?: true
- name: gym_gazebo
  version: master
  parameters:
    external?: true
- name: inception_weights_download
  version: v0.5
  parameters:
    external?: true
- name: resnet_weights_download
  version: v0.2
  parameters:
    external?: true
