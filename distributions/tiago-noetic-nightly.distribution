catalog:
  title: Tiago Software
  version: noetic-nightly
  replication: |
    You need to install [Basedeps](/distribution/tiago-basedeps-cuda11.6.xml) and
    [Tiago Simulation](/distribution/tiago-noetic-sim-minimal.xml) to use this distribution.

  platforms:
    - "ubuntu focal x86_64" 

variables:
  description: Clean Up robot software ontop of Tiago Noetic Dist
  recipe.maintainer:
    - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>

  toolkit.volume:    /vol/tiago/noetic
  toolkit.dir:       ${toolkit.volume}/nightly

  access: private
  ros.lang.disable: [geneus,genlisp]

  ros.underlay: ${toolkit.volume}/sim
  ros: noetic
  python.version: 3
  cudagen: 0
  cuda.arch: "6.1;8.6"
  java.jdk: Java11

  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${toolkit.dir}
    - CUDA_NVCC_FLAGS="--expt-relaxed-constexpr"
    - '@{next-value|[]}'

  shell.environment:${mode}:
    - CMAKE_PREFIX_PATH="${toolkit.dir}:\${CMAKE_PREFIX_PATH}"
    - PATH="${toolkit.dir}/bin:\${PATH}"
    - ROS_MAVEN_REPOSITORY=file://${toolkit.dir}/share/repository/
    - ROS_MAVEN_DEPLOYMENT_REPOSITORY=${toolkit.dir}/share/repository/
    - CXXFLAGS="-march=core2"
    - '@{next-value|[]}'

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p ${toolkit.dir}
    cp -a ${base-deps.dir}/* ${toolkit.dir}/

    for d in include lib/python3/dist-packages bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    cd ${toolkit.dir}
    ln -s lib lib64
    cd lib 
    ln -s python3 python3.8
    cd python3
    ln -s dist-packages site-packages

  cuda-version: ${next-value|11.6}
  #cuda.host.compiler: \$(which g++-9)
  base-deps.dir: ${toolkit.volume}/cuda${cuda-version}

include:
  - stacks/tiago/grasping-master
  - stacks/tiago/yolox
  - stacks/robocup/tiago-software-base-noetic
  #- stacks/tiago/people

versions:

- maven-repo-cleanup@master

# Deploy Volume to Robot
- name: deploy-volume
  version: master
  parameters:
    user: pal
    group: pal
    remote-host: tiago-47c

###########################################
## Autostart in clf pal_startup scripts
## (see wt folder from tiago_config_files)
###########################################
- ros-tiago_47_pkgs@master

# Clf startup stuff
- ros-tobi_sim@master
- ros-pose_publisher@master

# Admittance Control

- tiago_cupro_models@master

#######################################
## Fixes
#######################################

## Override pal joy_teleops with upstream pkg
#- ros-teleop_tools@kinetic-devel

#######################################
## ECWM
#######################################

- name: ecwm
  version: master
  parameters:
    cmake.options: 
      - DISABLE_CLANG_TIDY=ON
      - '@{next-value|[]}'

- cupro_sim@master
- ecwm_data@master
- ecwm_robocup@master
- tiago_mtc_tasks@master

## deps



#######################################
## Other
#######################################
- ros-groceries_pick_server@master
- ros-garbage_grasping@master
- ros-obstacle_detector@master

#######################################
# People
#######################################
- face_gender_age_detection@master

# bayes tracker + leg detect
- bayestracking@tiago_upstream
- bayes_people_tracker@master
- ros-people@clf
- detector_msg_to_pose_array@clf

- cftldROS_tracking@tiago_upstream

#######################################
# Reinstall from sim 
#######################################
# sim # gazebo_plugins ros_actor_cmd_vel_plugin


##### FIXes
#build upstream to match updated msgs we need for bonsai
- ros-navigation@noetic-devel

## More sim

- ros-rc-sim@master
- gazebo_ycb@master
- ros-gazebo_actor_vel_cmd_plugin@master
- ros-gazebo_actor_vel_cmd_plugin_msgs@master


#######################################
# Nightly New Stuff
#######################################

# Whisper
- pip-openai-whisper@master
- name: whisper-models
  versions:
  - version: base.en
  - version: small.en
  - version: medium
- ros-ros_whisper@master

# Vsok
- pip-vosk@master
- name: vosk-models
  versions:
  - version: vosk-model-small-en-us-0.15
  - version: vosk-model-en-us-0.22
  - version: vosk-model-spk-0.4
#- ros-ros_vosk@master

# Photogrammetry

- robocup_photogrammetry@master

- agni_tf_tools@master
- ros-clf_object_recognition_3d@yolox
- ros-clf_object_recognition_rviz@yolox

- ros-bio_ik@master
- ros-qr_detector@master


- ros-robocup_sound_play@master
- name: pip-install
  versions:
  - version: robocup_sound_play
    parameters:
      pip.packages:
        - "simpleaudio"
