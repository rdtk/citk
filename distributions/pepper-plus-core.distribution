# TODO: in the near future this distribution will be decomposed into [core, navigation, vision]

catalog:
  title: Pepper Plus Core
  version: current

variables:
  access: private
  recipe.maintainer:
  - flier@techfak.uni-bielefeld.de

  ros: kinetic
  # Disable catkin message generation for ...
  ros.lang.disable: [geneus,genlisp,genjava]
  # Disable testing targets ...
  make.test.targets: []

  # The default installation prefix
  toolkit.volume: /data/home/
  toolkit.dir: ${toolkit.volume}/pepper-plus-core

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}/opt";
    mkdir -p "${toolkit.dir}/bin";
    mkdir -p "${toolkit.dir}/lib";
    mkdir -p "${toolkit.dir}/etc";
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}"/bin

versions:

# SOFTBANK SDKs
- aldebaran-sdk-internal                                    @2.5.7.1
- aldebaran-choregraphe-internal                            @pepper-plus

# PEPPER CORE
- pepper-plus-vdemo-configs                                 @master
- pepper-plus-core                                          @master
- pepper-plus-sim                                           @master
- pepper-morse-data                                         @master
- pepper-morse-simcontroller                                @master
- pepper-plus-sim-bridge                                    @master
- pepper-naoqi-driver-py                                    @pepper-robocup
- pepper-naoqi-driver-cpp                                   @no_moveTo_call
- pepper-hwreport                                           @master

# NAVIGATION
- name: ros-navigation
  version: clf
  parameters:
    make.test.targets: []
- pepper-nav-bringup                                        @post_madgeburg_tuning
- pepper-nav-maps                                           @master
- ros-ira-laser-tools                                       @pre_magdeburg
- ros-laser_filters                                         @kinetic-devel-clf
- ros-filters                                               @lunar-devel
- ros-range_sensor_layer                                    @indigo
- ros-slam_gmapping                                              @hydro-devel

# VISION AND PECEPTION
- pcl                                                       @pcl-1.8.0
- dlib                                                      @v19.7
- cuda                                                      @9.0
- caffe                                                     @pepper-robocup
- cudnn                                                     @7.1
- tensorflow                                                @1.6.0
- opencv-plus-contrib-cuda                                 @3.3.1
- clf_perception_vision                                     @dlup_only
- pepper-perception-configs                                 @master
- cftldROS_tracking                                         @pepper-experimental
- dlib-python                                               @v19.7-cuda
- dataset-dlib                                              @dlib.net
- pepper-naoqi-pose-py                                      @stable-hash
- openpose_ros                                              @pepper_dev
- cmu-openpose                                              @v1.3.0
- ros-object-recognition-tensorflow-ros                     @detection
- ros-object-recognition-tensorflow-ros-rqt                 @detection
- ros-object-recognition-image-recognition-rqt              @detection
- ros-object-recognition-image-recognition-util             @detection
- ros-object-recognition-image-recognition-externals        @pepper-robocup-2018
- pepper-ros-image-viz                                      @master
- darknet_ros_ethz                                          @pepper_robocup
- bayestracking                                             @clf_ptracker_extended
- bayes_people_tracker                                      @clf_ptracker_extended
- ageitgey-face-recognition                                 @pepper-robocup
- pepper-plus-face-id                                       @master
- dataset-keras-gender_age                                  @28-3.73
- dataset-tensorflow-objects                                @1.0
- ros-image-transport-toggle                                @hydro-devel
- ros-tf-pose-estimation                                    @master
- clf_people_attribute_server                               @master

# SPEECHREC
- cmusphinx-sphinxbase                                      @master
- cmusphinx-pocketsphinx                                    @master
- pocketsphinx-adapter-catkin-ros                           @pepper-plus
- pepper-plus-psconfigs                                     @master
- pepper-plus-psgrammars                                    @master

# KNOWLEDGE BASE
- knowledge_base                                            @master

# MSGS
- ros-controls-control_msgs                                 @kinetic-devel
- naoqi_bridge_msgs                                         @pepper-robocup
- ros-object-tracking-msgs                                  @master
- ros-grasping-msgs                                         @augmented_pickplace
- openpose_ros_msgs                                         @pepper_dev
- ros-clf-perception-vision-msgs                            @master
- ros-openni2-tracker-msgs                                  @master
- bayes_people_tracker_msgs                                 @clf_ptracker_extended
- face_gender_age_detection_msgs                            @master
- ros-navigation_msgs                                       @clf
- knowledge_base_msgs                                       @master

# WEB
- rosbridge-server                                          @0.8.6
- rosapi                                                    @0.8.6
- rosbridge-library                                         @0.8.6
- ros-auth                                                  @develop

# HOLOLENS
- python-mqtt                                               @1.3.1
- rospy-message-converter                                   @0.4.0

# TESTING
- fsmt                                                      @0.5
- pyscxml                                                   @v.0.8.5-fsmt

# BEHAVIOR
- flexbe_app                                                @pepper-plus
- flexbe_behavior_engine                                    @master
- pepper-plus-behaviors                                     @master
- ros-smach                                                 @indigo-devel

# DEPENDENCIES
- python3-rospkg                                            @1.0.37
- python3-catkin-pkg                                        @0.2.10
- vdemo                                                     @pepper-plus

# MISC
- robocup-robosync                                          @pepper-plus

# EXPERIMENTAL
# - hyperion-core@master
# - hyperion-ui-extension@master
# - hyperion-graph-extension@master

# SIMPLE GRASP [not yet required]
# - pepper-grasp-ml-ros                                     @master
# - tfmodellib                                              @master
