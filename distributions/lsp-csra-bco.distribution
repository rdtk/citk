catalog:
  title: LSP CSRA BCO
  version: ${variant}

variables:
  make.threads: '2'
  variant: 'bco'
  flavor: xenial
  datadir: ${toolkit.volume}/data/${flavor}/${distribution-name}/
  ros: kinetic
  rstexperimental.exit_on_ambiguity: '1'
  access: private

  toolkit.volume: /vol/csra/
  toolkit.dir: ${toolkit.volume}/releases/${flavor}/${distribution-name}

  filesystem.group/unix: csra

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    #!/bin/bash
    mkdir -p "${toolkit.dir}"/{bin,etc,opt,share,var}
    mkdir -p "${datadir}"
    ln -vfs -T "${datadir}" "${toolkit.dir}/share/data"
    chgrp ${filesystem.group/unix} "${toolkit.dir}"
    chgrp ${filesystem.group/unix} "${toolkit.dir}/var"
    chmod u=rwx,go=rx "${toolkit.dir}"
    chmod u=rwx,g=rwxs,o=rx "${toolkit.dir}/var"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    chgrp -R ${filesystem.group/unix} "${toolkit.dir}"
    chmod -R u=rwX,go=rX "${toolkit.dir}"
    chmod -R g+w "${toolkit.dir}/var"
    find "${toolkit.dir}/var" -type d -exec chmod g+s {} \;

  java.jdk: Java11
  bco.registry.db.overwrite: 'TRUE'
  bco.registry.db.git.path: master
  maven.dependency-versions:
  - dependency.rsx.version=[0.18,0.19-alpha)

  gstreamer-version: '1.14'

versions:

- azul-zulu-openjdk                                         @11.0.2

- spread                                                    @4.4
- spread-python                                             @1.5spread4

- sbcl-binary                                               @1.4.10
- quicklisp                                                 @2018-08-31
- cl-launch                                                 @4.1.3-for-sbcl-binary
- cl-iterate-sequence                                       @hash-e116f4a
- cl-architecture.service-provider                          @0.5
- cl-architecture.builder-protocol                          @0.9
- cl-network.spread                                         @0.3
- cl-protobuf                                               @0.2
- cl-serialization.protocol-buffer                          @master

- rsc                                                       @0.18
- rsb-protocol                                              @0.18
- rsb-cpp                                                   @0.18
- rsb-cl                                                    @0.18
- rsb-python                                                @0.18
- rsb-matlab                                                @0.18
- rsb-tools-cpp                                             @0.18
- name: rsb-tools-cl
  versions:
  - version: 0.18-feature-norman-bridge
  - version: 0.18-rsb-only
- rsb-spread-cpp                                            @0.18
- rsb-xml-cpp                                               @0.18
- rsb-xml-java                                              @0.18
- rsb-xml-python                                            @0.18
- rst-proto                                                 @0.18-without-java
- rst-converters-cpp                                        @0.18
- rst-converters-python                                     @0.18
- name: rsbag-cl
  versions:
  - version: feature-online-size-tracking-0.15
  - version: '0.18'
- rsbag-tools-cl                                            @0.18-rsb-only
- rsb-process-monitor                                       @hash-2.0

- vdemo                                                     @master
- lsp-csra-system-startup                                   @master
- lsp-csra-system-config                                    @master
- libinkg                                                   @master
- inkg-rsb-interface                                        @master
- csra-utils                                                @master
- study-control                                             @master

- rst-experimental-proto                                    @0.18-without-java
- openbase-rct-java                                         @master
- rta-lib                                                   @master
- rst-utils                                                 @master
- apache-jena-fuseki-server                                 @3.7.0
- openbase-developer-tools                                  @latest-stable
- jps                                                       @latest-stable
- openbase-type-java                                         @master
- openbase-type-cpp                                         @master
- jul                                                       @master
- bco.authentication                                        @master
- bco.registry                                              @master
- bco.registry.editor                                       @master
- bco.registry.csra-db-deployer                             @master
- bco.dal                                                   @master
- openbase-bco-device                                       @master
- openbase-bco-app                                          @master
- openbase-bco-openapi                                      @master
- bco.ontology                                              @master
- bco.psc                                                   @master
- generic-display                                           @master
- bco.bcozy                                                 @master
- bco.stage                                                 @master
- bco.eveson                                                @master
- eclipse.smarthome.binding.bco                             @master
- pamini                                                    @1.17
- dialogflow                                                @master
- jsgf-parser                                               @master
- speech-command-proposer                                   @master
- highlight-service                                         @master
- location-light                                            @master
- floor-base                                                @master
- floor-lib                                                 @master
- floor-view                                                @master
- floor-input-emulator                                      @master


