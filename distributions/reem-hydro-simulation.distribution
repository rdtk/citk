catalog:
  title: REEM Simulation
  version: iros2015

variables:
  recipe.maintainer:
  - Jonathan Weisz <jweisz@cs.columbia.edu>

  platform-requires:
    precise:
      packages:
      - ros-hydro-desktop
      - ros-hydro-gazebo-ros
      - gazebo
      - gazebo-dbg
      - ros-hydro-object-recognition-ros
      package-repository:
      - name: ros-hydro
        install-script:
        - echo 'deb http://packages.ros.org/ros/ubuntu precise main' > /etc/apt/sources.list.d/ros-latest.list
        - apt-key adv --keyserver hkp://pool.sks-keyservers.net --recv-key 0xB01FA116
    ubuntu:
      packages:
      - blender
      - git
      - cmake
      - build-essential
      - python-setuptools
      - curl
      - tmux
      - htop
      - gedit
      - ros-hydro-desktop
      - python-scipy

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find ${toolkit.dir} -type d -exec chmod 2755 {} \;
    find ${toolkit.dir} -type f -exec chmod go+r {} \;
    find ${toolkit.dir} -type f -perm /100 -exec chmod go+x {} \;

versions:
- fsmt                                                      @0.17
- pyscxml                                                   @v.0.8.4-fsmt
- robobench-exp-reem-experiments                            @master
- runnable-robobench-reem-patrolling-profile                @master
- runnable-robobench-reem-grasping-profile                  @master
- oprofile                                                  @master
- reem-simulation-sources                                   @robobench
- reem-sources                                              @robobench
- robobench-exp-tools                                       @master
