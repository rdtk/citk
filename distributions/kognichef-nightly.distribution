catalog:
  title: Kognichef
  version: nightly

variables:
  access: private

  platform-requires:
    ubuntu:
      packages:
      - cmake
      - build-essential
      - libprotobuf-dev
      - protobuf-compiler
      - libboost-dev
      - python-setuptools
      - python-dev
      - python-protobuf
      - libprotobuf-java
      - libboost-thread-dev
      - libboost-filesystem-dev
      - libboost-signals-dev
      - libboost-program-options-dev
      - libboost-system-dev
      - libboost-regex-dev
      - maven
      - openjdk-7-jdk
      - openjdk-8-jdk
      - openjfx
      - libpcl-all
      - nodejs
      - nodejs-legacy
      - npm
      - libffi-dev

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}"

versions:
- kognidoor-ble-grabber                                     @chef
- kognidoc                                                  @master
- kogniserver                                               @master
- kogniperception-kognichef                                 @master
- kognichef-drawer-control                                  @master
- kognichef-object-detection                                @master
- kognichef-fillrate-detection                              @master
- kognichef-presence-detection                              @master
- kognichef-fillrate-server                                 @master
- kognichef-kinect-server                                   @master
- kognichef-forcesensor-server                              @master
- kognichef-frontend                                        @master
- kogniconfig                                               @master
- kognihome-tools                                           @master
- kognicom                                                  @master
- yaml-cpp                                                  @release-0.5.3
- kognirecipes                                              @master
- tts_recipe                                                @master
- mieleappliances                                           @master
- kognichef-controller                                      @master
- icl-kognihome                                             @trunk
- bullet                                                    @master
- comedilib                                                 @master
- vdemo                                                     @master
- inprotk                                                   @csra_mary5
- marytts-timobaumann                                       @incrementalityChangesTake2
- marytts-voices                                            @5.1
- kognichef-dialogflow                                      @master
- kognichef-inprotk-conf                                    @master
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- rsc                                                       @0.15
- rsb-protocol                                              @0.15
- rsb-cpp                                                   @0.15
- rsb-python                                                @0.15
- rsb-java                                                  @0.15
- rsb-matlab                                                @0.15
- rsb-spread-cpp                                            @0.15
- rsb-gstreamer                                             @release-0.15
- rsb-opencv                                                @refactoring
- rst-proto-csra-workaround                                 @0.15
- rst-proto-kognihome                                       @0.15
- rst-converters-cpp                                        @0.15
- rst-converters-python                                     @0.15
- rsb-xml-cpp                                               @0.15
- rsb-xml-java                                              @0.15
- rsb-xml-python                                            @0.15
- rsb-scxml-engine                                          @csra-0.15
- rsb-tools-cpp                                             @0.15
- sbcl                                                      @sbcl-1.3.11
- quicklisp                                                 @2016-10-31
- cl-launch                                                 @4.1.3
- cl-iterate-sequence                                       @master
- cl-architecture.service-provider                          @release-0.1
- cl-architecture.builder-protocol                          @release-0.5
- cl-network.spread                                         @release-0.2.1
- rosetta-cl                                                @0.2
- rosetta-esrap-util-cl                                     @0.2
- rosetta-yarp-cl                                           @0.2
- rosetta-ros-cl                                            @0.2
- rsb-cl                                                    @0.15
- cl-protobuf                                               @0.1
- rsbag-cl                                                  @0.15
- rsb-ros-cl                                                @0.15
- rsb-yarp-cl                                               @0.15
- rsb-tools-cl                                              @0.15
- rsbag-tools-cl                                            @0.15
- dlib                                                      @v19.2
- gazetool                                                  @nogui-rsb
- controller-visualisation                                  @master
