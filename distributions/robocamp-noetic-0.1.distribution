catalog:
  title: ADHS Robocamp
  version: ${variant}

variables:
  recipe.maintainer:
  - bcarlmey@techfak.uni-bielefeld.de
  access: private
  make.threads: '2'
  variant: '0.1'

  toolkit.volume: /vol/robocamp/
  toolkit.dir: ${toolkit.volume}/releases/${distribution-name}

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}" ; chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: chmod -R g+x "${toolkit.dir}"/bin

  ros: ${next-value|noetic}
  python.version: ${next-value|3}
  
  cuda-version: ${next-value|10.1}
  cuda.dir: /vol/cuda/${cuda-version}
  cuda.host.compiler: \$(which g++-8)
  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${cuda.dir}
    - CUDA_NVCC_FLAGS="--expt-relaxed-constexpr"
    - '@{next-value|[]}'
    
  unity-version: ${next-value|2020.3.24f1}
  unity.dir: /vol/unity/${unity-version}/bin
      
versions:
- vdemo                                                     @master
- libreflexxes                                              @1.2.6
- humotion                                                  @master
- xsc3                                                      @master
- hlrc_server                                               @robocamp
- hlrc_client_cpp                                           @robocamp
- hlrc_client_python                                        @robocamp
- hlrc_tts_provider                                         @robocamp
- marytts                                                   @v5.2beta2
- marytts-voices                                            @5.1
- flobi-description                                         @master
- floka-tcp-connector                                       @main
- floka-unity-sim                                           @main

- open-cv                                                   @2.4
- gaze-detector                                             @jenkins
- opencv                                                    @4.1.0
- dlib                                                      @v19.14
- openface-ros                                              @master

- robocamp-startup                                          @noetic
- robocamp-gui                                              @noetic
- robocamp-response-cost-system                             @master
- robocamp-utils                                            @main
- robocamp-background-behavior                              @main
- robocamp-psychopy-gui                                     @noetic
