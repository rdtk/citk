catalog:
  title: ADHS Robocamp
  version: ${variant}

variables:
  recipe.maintainer:
  - bcarlmey@techfak.uni-bielefeld.de
  access: private
  make.threads: '2'
  variant: '0.1'

  toolkit.volume: /vol/robocamp/
  toolkit.dir: ${toolkit.volume}/releases/${distribution-name}

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}" ; chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: chmod -R g+x "${toolkit.dir}"/bin

  ros: ${next-value|melodic}


  cuda-version: ${next-value|9.0}
  cuda.dir: /vol/cuda/${cuda-version}
  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${cuda.dir}
    - '@{next-value|[]}'

    
  unity-version: ${next-value|2020.3.24f1}
  unity.dir: /vol/unity/${unity-version}/bin

versions:
- opencv                                                    @4.1.0
- dlib                                                      @v19.14
- openface-ros                                              @master
- gaze-detector                                             @jenkins
- vdemo                                                     @master
- libreflexxes                                              @1.2.6
- humotion                                                  @master
- flobi-description                                         @hash-77eec938
- robocamp-startup                                          @main
- xsc3                                                      @master
- hlrc_server                                               @robocamp
- hlrc_client_cpp                                           @robocamp
- hlrc_client_python                                        @robocamp
- hlrc_tts_provider                                         @robocamp
- marytts                                                   @v5.2beta2
- marytts-voices                                            @5.1
- ximea-api                                                 @master
- ximea-camera                                              @devel-indigo
- flobi-sim                                                 @master
- python3-rospkg                                            @1.0.37
- python3-catkin-pkg                                        @0.4.24
- robocamp-gui                                              @noetic
- robocamp-response-cost-system                             @noetic
- floka-tcp-connector                                       @main
- floka-unity-sim                                           @main
- robocamp-utils                                            @main
- flobi-background-behavior                                 @main
